package juliaTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;

import com.juliasoft.julia.api.engine.build.AnalysisEngineException;
import com.juliasoft.julia.api.engine.utils.options.Optionable.OptionsException;
import com.juliasoft.julia.api.engine.warning.JuliaWarningMessage;
import com.juliasoft.julia.checks.InternalChecker;
import com.juliasoft.julia.checks.butterfly.ButterflyChecker;
import com.juliasoft.julia.engine.InternalOptions;
import com.juliasoft.julia.engine.Julia;
import com.juliasoft.julia.frameworks.ApplicationCreationException;
import com.juliasoft.julia.utils.ui.TextGfx;

public class Main {

	public static void main(String[] args) {
		
		//String checker = "Butterfly";
		String pathProject = "C:\\Users\\flavio\\AndroidStudioProjects\\MyApplication\\app\\build\\outputs\\apk\\app-debug.apk";
		//String pathProject = "C:\\Users\\flavio\\testJarZip.jar";
		
		String[] arrPathProject = pathProject.split("\\\\");
		
		String fileName = arrPathProject[arrPathProject.length-1];

		String androidTestPath = "C:\\Users\\flavio\\androidTesting\\" + fileName;

		System.out.println("[STEP 1]: copying APK into workdirectory");
		
		try {
			FileUtils.copyFile(new File(pathProject), new File(androidTestPath));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} 
		
		System.out.println("[STEP 2]: decompiling APK into JAR file");
		
		String androidTestPathOutput = "C:\\Users\\flavio\\androidTesting\\" + fileName.split("\\.")[0] + ".jar";
		
		try {
			Process p = new ProcessBuilder("C:\\dex2jar\\d2j-dex2jar.bat", androidTestPath,"--force", "-o", androidTestPathOutput).start();
			
			p.waitFor();
			
			if(p.exitValue() != 0) {
				System.out.print("Decompile problem");
			}
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("[STEP 3]: analyzing JAR with Julia");
		
		InternalOptions options = null;
		try {
			options = new InternalOptions(new String[] { "-i", "java.", "-i", "android.", "-framework", "androidAPI19", "-mergeArrays", "-Warnings"});
		} catch (OptionsException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		LinkedList<InternalChecker> checkers = new LinkedList<InternalChecker>();
		
		checkers.add(new ButterflyChecker());
		
		Julia julia = new Julia(options, checkers, new TextGfx(new PrintWriter(System.out), options));
	
		List<InputStream> projects = new ArrayList<InputStream>();
		try {
			projects.add(new FileInputStream(new File(androidTestPathOutput)));
			//projects.add(new FileInputStream(new File(pathProject)));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("Error to open .jar file");
			System.exit(1);
		}
		List<InputStream> dependences = new ArrayList<InputStream>();

		try {
			julia.run(projects, dependences);
		} catch (ApplicationCreationException | AnalysisEngineException e) {
			e.printStackTrace();
			System.exit(1);
		}
	
		SortedSet<JuliaWarningMessage> juliaWarnings = julia.getJuliaWarnings();
		
		for(JuliaWarningMessage warning: juliaWarnings) {
			System.out.println(warning.toString());
		}
		
		System.out.println("THE END!");
	}
	
	

}